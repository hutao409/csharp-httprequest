﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.entity
{
    public class Resp
    {
        /// <summary>
        /// 
        /// </summary>
        public string errText { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int nRows { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ResultSetsItem> resultSets { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int rtnCode { get; set; }

    }
}
