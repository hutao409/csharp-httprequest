﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.entity
{
    public class EmployeeVo
    {
        /// <summary>
        /// 
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string createUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string createTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string lastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string enable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string lastModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string cardNo { get; set; }
        /// <summary>
        /// 研发
        /// </summary>
        public string position { get; set; }
        /// <summary>
        /// 信息部
        /// </summary>
        public string department { get; set; }
    }

}
