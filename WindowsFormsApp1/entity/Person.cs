﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.entity
{
    public class Person
    {
        /// <summary>
        /// 
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string createUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string createTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string lastModifyUserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string enable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string lastModifyTime { get; set; }
        /// <summary>
        /// 胡涛
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string pwd { get; set; }
        /// <summary>
        /// 男
        /// </summary>
        public string sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string phone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string employeeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public EmployeeVo employeeVo { get; set; }
    }
}
