﻿using EasyHttp.Http;
using EasyHttp.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using WindowsFormsApp1.entity;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private string fileName;
        private int requestType;

        public Form1()
        {
            InitializeComponent();
        }

        private void aboutMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("这是一个测试网络请求Demo");
        }

        private void get_Button_Click(object sender, EventArgs e)
        {
            requestType = 1;
            toolStripProgressBar1.Style = ProgressBarStyle.Marquee;
            //request();
            backgroundWorker1.RunWorkerAsync();
        }
        private void post_Button_Click(object sender, EventArgs e)
        {
            requestType = 2;
            toolStripProgressBar1.Style = ProgressBarStyle.Marquee;
            //request();
            backgroundWorker1.RunWorkerAsync();
        }

        private void select_File_Button_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = "f:\\";//注意这里写路径时要用c:\\而不是c:\
            openFileDialog1.Filter = "文本文件|*.*|C#文件|*.cs|所有文件|*.*";
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.FilterIndex = 1;
            //openFileDialog1.ShowDialog();
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                fileName = openFileDialog1.FileName;
                //File fileOpen = new File(fName);

            }
        }

        private void upload_File_Button_Click(object sender, EventArgs e)
        {
            if (fileName == null || fileName.Length == 0)
            {
                MessageBox.Show("请选择一个文件");
                return;
            }
            requestType = 3;
            toolStripProgressBar1.Style = ProgressBarStyle.Marquee;
            //request();
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            switch (requestType) {
                case 1:
                    requestGet();
                    break;
                case 2:
                    requestPost();
                    break;
                case 3:
                    requestFile();
                    break;

            }
        }

        //网络请求
        private void requestGet()
        {

            try
            {

                //var url = "http://192.168.251.38:9080/WCSVR/WebFunctionInterface";
                var url = "http://10.0.0.174:8080/pep/sys/person";
                var http = new HttpClient();
                http.Request.Accept = HttpContentTypes.ApplicationJson;
                http.Request.Timeout = 3000;
                var response = http.Get(url, HttpContentTypes.ApplicationXWwwFormUrlEncoded);
                
                var json = response.RawText;
                //var result = JsonConvert.DeserializeObject<Resp>(json);
                var result = JsonConvert.DeserializeObject<List<Person>>(json);
                Console.WriteLine("json: {0}", json);
                //异步更新Ui
                Invoke(new Action(() =>
                {
                    resultTextBox.Text = json;
                    netwrokToolStripStatusLabel.Text = "网络状态：" + response.StatusCode;
                    notifyIcon1.Visible = true;
                    notifyIcon1.BalloonTipTitle = "提示";
                    notifyIcon1.BalloonTipText = "网络状态：" + response.StatusCode;
                    notifyIcon1.ShowBalloonTip(30);
                    toolStripProgressBar1.Style = ProgressBarStyle.Blocks;
                    listView1.Items.Clear();
                    listView1.BeginUpdate();
                    foreach (Person p in result)
                    {
                        ListViewItem item = new ListViewItem();
                        item.Text = p.name + "  " + p.phone;
                        item.SubItems.Add(p.name);
                        item.SubItems.Add(p.phone);
                        listView1.Items.Add(item);
                    }
                    listView1.EndUpdate();
                }));

            }
            catch (Exception e)
            {
                Invoke(new Action(() => {
                    toolStripProgressBar1.Style = ProgressBarStyle.Blocks;
                    notifyIcon1.Visible = true;
                    notifyIcon1.BalloonTipTitle = "提示";
                    notifyIcon1.BalloonTipText = "网络状态：" + e.Message;
                    notifyIcon1.ShowBalloonTip(30);
                    netwrokToolStripStatusLabel.Text = "网络状态：" + e.Message;

                }));

            }
        }


        private void requestPost()
        {

            try
            {

                string funcjson = "{\"args\":[111329L,\"2020-06-28 00:00:00\",\"2020-07- 29 23:59:59\", 19940L],\"className\":\"tms.service.logic.ScheduleService\",\"funcName\":\"getHistorySubackLstAndBoxByCustom\",\"ifInstance\":true}";
                Dictionary<String, String> valuePairs = new Dictionary<String, String>
                {
                    { "funcjson", funcjson }
                };

                //var url = "http://192.168.251.38:9080/WCSVR/WebFunctionInterface";
                //var url = "http://10.0.0.174:8080/pep/sys/person";

                //var url = "http://127.0.0.1:8080/upload";
                var url = "http://10.0.0.174:8080/pep/file";
                var http = new HttpClient();

                http.Request.Accept = HttpContentTypes.ApplicationJson;
                http.Request.Timeout = 3000;
                http.Request.ContentType = HttpContentTypes.MultiPartFormData;
                List<FileData> list = new List<FileData>();
                FileData fileData = new FileData();

                //fileData.Filename = @"C:\Users\cdfy\Pictures\按品送货\分拨明细.png";
                //var imagePath = AppDomain.CurrentDomain.BaseDirectory + @"test.jpg";
                fileData.Filename = fileName;
                //fileData.ContentType = "image/jpeg";
                //fileData.ContentTransferEncoding = "utf-8";
                fileData.FieldName = "file";
               
                list.Add(fileData);
                //http.Request.MultiPartFileData = list;
                //http.Request.MultiPartFormData = null;

                //var response = http.Post(url, valuePairs, HttpContentTypes.ApplicationXWwwFormUrlEncoded);
                //var response = http.Post(url, null, HttpContentTypes.MultiPartFormData);
                var response = http.Post(url, null, list);
                //var response = http.PutFile(url, fileName, HttpContentTypes.MultiPartFormData);
                var json = response.RawText;
                //var result = JsonConvert.DeserializeObject<Resp>(json);
                //var result = JsonConvert.DeserializeObject<List<Person>>(json);
                Console.WriteLine("json: {0}", json);
                //异步更新Ui
                Invoke(new Action(() =>
                {
                    resultTextBox.Text = json;
                    netwrokToolStripStatusLabel.Text = "网络状态：" + response.StatusCode;
                    notifyIcon1.Visible = true;
                    notifyIcon1.BalloonTipTitle = "提示";
                    notifyIcon1.BalloonTipText = "网络状态：" + response.StatusCode;
                    notifyIcon1.ShowBalloonTip(30);
                    toolStripProgressBar1.Style = ProgressBarStyle.Blocks;
/*                    listView1.Items.Clear();
                    listView1.BeginUpdate();
                    foreach (Person p in result)
                    {
                        ListViewItem item = new ListViewItem();
                        item.Text = p.name + "  " + p.phone;
                        item.SubItems.Add(p.name);
                        item.SubItems.Add(p.phone);
                        listView1.Items.Add(item);
                    }
                    listView1.EndUpdate();*/
                }));

            }
            catch (Exception e)
            {
                Invoke(new Action(() => {
                    toolStripProgressBar1.Style = ProgressBarStyle.Blocks;
                    notifyIcon1.Visible = true;
                    notifyIcon1.BalloonTipTitle = "提示";
                    notifyIcon1.BalloonTipText = "网络状态：" + e.Message;
                    notifyIcon1.ShowBalloonTip(30);
                    netwrokToolStripStatusLabel.Text = "网络状态：" + e.Message;

                }));

            }
        }

        private void requestFile()
        {

            try
            {

                //var url = "http://127.0.0.1:8080/upload";
                var url = "http://10.0.0.174:8080/pep/file";
                var http = new HttpClient();

                http.Request.Accept = HttpContentTypes.ApplicationJson;
                http.Request.Timeout = 3000;
                http.Request.ContentType = HttpContentTypes.MultiPartFormData;
                List<FileData> list = new List<FileData>();
                FileData fileData = new FileData();
                //fileData.Filename = @"C:\Users\cdfy\Pictures\按品送货\分拨明细.png";
                //var imagePath = AppDomain.CurrentDomain.BaseDirectory + @"test.jpg";
                fileData.Filename = fileName;
                //fileData.ContentType = "image/jpeg";
                //fileData.ContentTransferEncoding = "utf-8";
                fileData.FieldName = "file";
                list.Add(fileData);
                var response = http.Post(url, null, list);
                var json = response.RawText;
                Console.WriteLine("json: {0}", json);
                //异步更新Ui
                Invoke(new Action(() =>
                {
                    resultTextBox.Text = json;
                    netwrokToolStripStatusLabel.Text = "网络状态：" + response.StatusCode;
                    notifyIcon1.Visible = true;
                    notifyIcon1.BalloonTipTitle = "提示";
                    notifyIcon1.BalloonTipText = "网络状态：" + response.StatusCode;
                    notifyIcon1.ShowBalloonTip(30);
                    toolStripProgressBar1.Style = ProgressBarStyle.Blocks;
                }));

            }
            catch (Exception e)
            {
                Invoke(new Action(() => {
                    toolStripProgressBar1.Style = ProgressBarStyle.Blocks;
                    notifyIcon1.Visible = true;
                    notifyIcon1.BalloonTipTitle = "提示";
                    notifyIcon1.BalloonTipText = "网络状态：" + e.Message;
                    notifyIcon1.ShowBalloonTip(30);
                    netwrokToolStripStatusLabel.Text = "网络状态：" + e.Message;

                }));

            }
        }

    }
}
